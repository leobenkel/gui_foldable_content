GuiFoldableContent.configure do |config|
	config.default_close_indicator = "UP"
	config.default_open_indicator = "DOWN"
end
