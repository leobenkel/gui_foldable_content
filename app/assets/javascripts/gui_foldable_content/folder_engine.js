

$(document).ready(function(){
	$('.gui_foldable_area').each(function(i, elem){
		var default_state = $(elem).data().default_foldable_state
		$(elem).find('.gui_foldable_area_title').each(function(i, elem){
			if (default_state) {
				$(elem).find('.gui_foldable_indicator_open').show();
				$(elem).find('.gui_foldable_indicator_close').hide();
			} else {
				$(elem).find('.gui_foldable_indicator_close').show();
				$(elem).find('.gui_foldable_indicator_open').hide();
			}
		});
		$(elem).find('.gui_foldable_content').each(function(i, elem){
			$(elem).toggle(default_state);
			$(elem).data().foldable_state = default_state
		});
	});
})

toggle_gui_divs = function(div_hidding, div_showing){
	var hidden_opacity = 0.3;
	var fading_speed = 'fast';

	$(div_hidding).stop(true,true);
	$(div_showing).stop(true,true);

	$(div_hidding).fadeTo(fading_speed, hidden_opacity,'linear', function(){
		$(div_showing).css('opacity', hidden_opacity);
		$(div_hidding).hide();
		$(div_showing).show();
		$(div_showing).fadeTo(fading_speed, 1);
	});
}

toggle_gui_foldable_content = function(title){

	var foldy = $(title).next('.gui_foldable_content');
	var state = foldy.data().foldable_state;

	var fading_options = {
			duration: 'fast',
			queue: false,
			easing: 'linear'
		};

	if (state) {
		$(foldy).fadeOut(fading_options);

		toggle_gui_divs($(title).find('.gui_foldable_indicator_open'), $(title).find('.gui_foldable_indicator_close'));
	} else {
		$(foldy).fadeIn(fading_options);
		toggle_gui_divs($(title).find('.gui_foldable_indicator_close'), $(title).find('.gui_foldable_indicator_open'));
	}

	foldy.data().foldable_state = !state;
}
