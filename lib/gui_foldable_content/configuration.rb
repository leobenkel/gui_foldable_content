module GuiFoldableContent
	class Configuration
		attr_accessor :default_close_indicator
		attr_accessor :default_open_indicator

		def initialize
			@default_close_indicator = "[C]"
			@default_open_indicator = "[O]"
		end
	end
end
