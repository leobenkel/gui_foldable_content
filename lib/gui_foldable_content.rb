require "gui_foldable_content/configuration"
require 'gui_foldable_content/view_helpers'

module GuiFoldableContent
	require 'gui_foldable_content/engine'

	class << self
		attr_accessor :configuration
	end
	def self.configuration
		@configuration ||= Configuration.new
	end
	def self.configure
		yield(configuration)
	end
end
